<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\profile;

class HomeController extends Controller
{

    public function utama()
    {
        return view('Halaman.utama');
    }

    public function tambahdata()
    {
        return view('Halaman.tambahdata');
    }

    public function insertdata(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        $query = DB::table('users')->insert([
            "name" => $request["name"],
            "email" => $request["email"],
            "password" => $request["password"],
        ]);
        return redirect('/data-create');
    }

    public function data()
    {
        // return view('Halaman.data-table');
        $data = DB::table('users')->get();
        return view('Halaman/data-table', compact('data'));
    }
    public function show($id)
    {
        $data = DB::table('users')->where('id', $id)->first();
        return view('halaman/show', compact('data'));
    }

    public function destroy($id)
    {
        $data = Profile::find($id);
        $data->delete();
        return redirect('/data-create');
    }
    public function edit($id)
    {
        $data = profile::find($id);
        return view('halaman/edit', [
            "title" => "Edit Data",
            "data" => $data
        ]);
    }
    public function update(Request $request, $id)
    {
        $data = profile::find($id);
        $data->name = $request->name;
        $data->email = $request->email;
        $data->password = $request->password;
        $data->save();

        return redirect('data-create');
    }
}
