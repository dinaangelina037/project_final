<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\bukucontroller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/utama', [HomeController::class, 'utama']);
Route::get('/insertdata', [HomeController::class, 'tambahdata']);
Route::get('/data-create', [HomeController::class, 'data']);
Route::post('/simpan', [HomeController::class, 'insertdata']);

// 
// crud create
Route::get('/buku/create', [bukucontroller::class, 'create']);


Route::get('/profile/{profile_id}', [HomeController::class, 'show']);
Route::DELETE('/profile/{profile_id}', [HomeController::class, 'destroy']);
Route::get('/profile/{profile_id}/edit', [HomeController::class, 'edit']);
Route::put('/profile/{profile_id}/update', [HomeController::class, 'update']);
