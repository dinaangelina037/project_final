@extends('layouts.index')
@section('title')
    Perpustakaan Nasional
@endsection

@section('content')
    <form action="/simpan" method="POST">
        @csrf
        <div class="form-group">
            <label for="">Nama Anggota</label>
            <input type="text" name="name" id="name" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input type="text" name="email" id="email" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Password</label>
            <input type="text" name="password" id="password" class="form-control">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>

    </form>
@endsection
