<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>halaman3</title>
</head>
<body>
    <h1>Pendaftaran Anggota Online</h1>
    <h2>Petunjuk Pengisian Form</h2>

    <ul>
        <li>Pastikan data yang anda masukan sesuai indentitas anda</li>
        <li>Silahkan hubungi bagian layanan Perpustakaan Nasional, jika anda pernah mendaftarkan diri sebelumnya namun akun anda tidak aktif.</li>
        <li>Inputan dengan tanda * wajib diisi.</li>
        <li>Klik disini, jika anda telah terdaftar sebagai anggota, namun belum memiliki user dan password akses layanan Keanggotaan</li>

        <li>Mendaftar di <a href="/insertdata">Form SignIn</a></li>
    </ul>
    
</body>
</html>