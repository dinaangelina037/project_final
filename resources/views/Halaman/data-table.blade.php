@extends('layouts.index')

@section('title')
    Daftar Laporan Perpustakaan
@endsection

@section('content')
    <a href="/insertdata" class="btn btn-primary btn-sm">Tambah</a>

    <table class="table">
        <thead>
            <tr>
                {{-- <th scope="col">#</th> --}}
                <th scope="col">Nama</th>
                <th scope="col">Email</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($data as $dt)
                <tr>

                    <td>{{ $dt->name }}</td>
                    <td>{{ $dt->email }}</td>
                    <td>
                        <a href="/profile/{{ $dt->id }}" class="btn btn-info">Show</a>
                        <a href="/profile/{{ $dt->id }}/edit" class="btn btn-primary">Edit</a>
                        <form action="/profile/{{ $dt->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger btn-xs">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
