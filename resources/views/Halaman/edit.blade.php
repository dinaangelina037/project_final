@extends('layouts.index')
@section('title')
    Perpustakaan Nasional
@endsection

@section('content')
    <form action="/profile/{{ $data->id }}/update" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="">Nama Anggota</label>
            <input type="text" name="name" id="name" value="{{ $data->name }}" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input type="text" name="email" id="email" value="{{ $data->email }}" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Password</label>
            <input type="text" name="password" id="password" value="{{ $data->password }}" class="form-control">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>

    </form>
@endsection
