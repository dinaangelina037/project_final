@extends('layouts.index')

@section('title')
Daftar Laporan Perpustakaan 
@endsection

@section('content')
<form>
  <div class="form-group">
    <label >Judul Buku</label>
    <input type="text" name="judul" class="form-control">
  </div>
  <div class="form-group">
    <label >Pengarang Buku</label>
    <input type="text" name="pengarang" class="form-control">
  </div>
  <div class="form-group">
    <label >Jenis Buku</label>
    <input type="text" name="jenis buku" class="form-control">
  </div>
  <div class="form-group">
    <label >Penerbit Buku</label>
    <input type="text" name="penerbit" class="form-control">
  </div>
  {{-- <div class="form-group">
    <label >Pengarang</label>
  </div> --}}
  {{-- <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div> --}}
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection